# Plug-ins y  scripts para The Gimp
Más información sobre los scripts en: [https://myc-git.gitlab.io/mycweb/](https://myc-git.gitlab.io/mycweb/)  
Instrucciones de instalación:  
**Scripts:** 
[https://docs.gimp.org/es/install-script-fu.html](https://docs.gimp.org/es/install-script-fu.html)  
***Para python:***
1.  Descargar y descomprimir (Si estuviera comprimido) para obtener el archivo con extensión .py
2.  Copie el archivo .py en su carpeta de plug-ins (si no sabe cual es, abra el Gimp y vaya a Editar/Preferencias &rarr; Carpetas &rarr; Complementos)
    *  En linux debe darle permisos de ejecución
3.  Reinicie el Gimp.

***

### selección_a_lápiz.scm:
Script creado a partir del siguiente tutorial: [http://memhet.wordpress.com/2012/02/18/gimp-papelito-y-fantasia/](http://memhet.wordpress.com/2012/02/18/gimp-papelito-y-fantasia/)  

Simula un dibujo a lápiz de la parte seleccionada de una imagen.

**Lenguaje:**  
scheme  

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/Filters/Artistic  
<code>&lt;</code>Imagen<code>&gt;</code>/Filtros/Artísticos

***

### texto_dorado.scm:

Crea un texto relleno de un mapa de degradado.

**Lenguaje:**  
scheme  


**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/
***

### textolunar.py:
Basado en el siguiente tutorial: [http://gimpchat.com/viewtopic.php?f=23&t=7530](http://gimpchat.com/viewtopic.php?f=23&t=7530)

Crea un texto con un relleno que simula la superficie de la luna.

**Lenguaje:**  
python

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/Python-Fu/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/Python-Fu/

***
### textopeludo.py:
Basado en el siguiente tutorial: [http://gimpchat.com/viewtopic.php?f=23&t=6652](http://gimpchat.com/viewtopic.php?f=23&t=6652)

Script para simular un texto peludo, o alfombrado.

**Lenguaje:**  
python

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/Python-Fu/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/Python-Fu/

***
### esponja.scm:
Basado en el siguiente tutorial: [http://gimpfun.blogspot.com.es/2008/07/spongy-text.html](http://gimpfun.blogspot.com.es/2008/07/spongy-text.html)

Script para simular un texto escrito en una esponja.

**Lenguaje:**  
scheme

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/

***

### asfalto.scm:
Crea un texto simulando una textura de asfalto.

**Lenguaje:**   
scheme

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/

***

### arrugado.scm:
Basado en los siguientes tutoriales: [http://mirincon-misaani.blogspot.com.es/2011/03/el-gimp-papel-viejo.html](http://mirincon-misaani.blogspot.com.es/2011/03/el-gimp-papel-viejo.html) y [http://mirincon-misaani.blogspot.com.es/2011/02/el-gimp-textura-papel-arrugado.html](http://mirincon-misaani.blogspot.com.es/2011/02/el-gimp-textura-papel-arrugado.html)

Crea un texto sobre un papel arrugado.

**Lenguaje:**  
scheme

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/

***

### imagen_arrugada.py
Basado en los siguientes tutoriales: [http://mirincon-misaani.blogspot.com.es/2011/03/el-gimp-papel-viejo.html](http://mirincon-misaani.blogspot.com.es/2011/03/el-gimp-papel-viejo.html) y [http://mirincon-misaani.blogspot.com.es/2011/02/el-gimp-textura-papel-arrugado.html](http://mirincon-misaani.blogspot.com.es/2011/02/el-gimp-textura-papel-arrugado.html)

Arruga una imagen.

**Lenguaje:**  
python

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/Filters/Decor  
<code>&lt;</code>Imagen<code>&gt;</code>/Filtros/Decorativos

***

### texto_luz.scm

Basado en el siguiente tutorial: [http://puteraaladin.blogspot.mx/2008/12/creating-light-text-effect-in-gimp.html](http://puteraaladin.blogspot.mx/2008/12/creating-light-text-effect-in-gimp.html)

**Lenguaje:**  
scheme

**Localización**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/

***
### animar_rotacion.py
Crea una rotación animada de una imagen.

**Lenguaje:**  
python

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/Filters/Animation/  
<code>&lt;</code>Imagen<code>&gt;</code>/Filtros/Animación/

***

### biselado.py
Basado en el sigiuente tutorial: [http://gimpchat.com/viewtopic.php?f=10&t=6268](http://gimpchat.com/viewtopic.php?f=10&t=6268)  
Crea un texto biselado.

**Lenguaje:**  
python

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/Python-Fu/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/Python-Fu/

***

### textoardiendo.py
Basado en el siguiente tutorial: [https://narodny-geroy.deviantart.com/art/Tutorial-Fireballs-with-Gimp-153793175](https://narodny-geroy.deviantart.com/art/Tutorial-Fireballs-with-Gimp-153793175)  
Simula un texto relleno de fuego.

**Lenguaje:**  
python

**Localización:**  
<code>&lt;</code>Image<code>&gt;</code>/File/Create/Python-Fu/  
<code>&lt;</code>Imagen<code>&gt;</code>/Archivo/Crear/Python-Fu/

***

### utilidades_animacion.py
Conjunto de acciones sobre múltiples capas que pueden ser útiles para una animación.  
Invertir el orden de las capas y renombrarlas.

**Lenguaje:**  
python

**Localización:**  
/Filters/Multicapas/  
/Filtros/Multicapas/

***

### capas_transparencia.py
Conjunto de acciones sobre el canal alfa y la transparencia en múltiples capas.  
Añadir o eliminar el canal alfa, color a alfa, semiaplanar, umbral alfa, sustituir un color por atro o por transparencia y rellenar transparencia con un color.

**Lenguaje:**  
python

**Localización:**  
/Filters/Multicapas/  
/Filtros/Multicapas/

***

### capas_utilidades_varias.py
Conjunto de acciones sobre múltiples capas.  
Renombrar todas las capas y guardarlas cada una como una imagen independiente.

**Lenguaje:**  
python

**Localización:**  
/Filters/Multicapas/  
/Filtros/Multicapas/

***