#!/usr/bin/env python
# -*- coding: utf-8 -*-

# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *

def error(filtro, modo, imagen):
	pdb.gimp_message("No es posible aplicar " + filtro + " a imágenes del tipo " + modo)


def inicio(imagen, alfa, colorAlfa, color, semi, umbral, nivelUmbral, limpiar, difuminar, radio, colorBorrar, colorRelleno):

	pdb.gimp_image_undo_group_start(imagen)
	pdb.gimp_context_push()
	pdb.gimp_context_set_foreground (colorRelleno)
	pdb.gimp_context_set_background (colorRelleno)
	capas = imagen.layers

	tupla = ("color RGB", "escala de grises", "color indexado")
	modo = pdb.gimp_image_base_type(imagen)
	tipo = tupla[modo]


	if alfa == "si":
		for i in capas:
			pdb.gimp_layer_add_alpha(i)
	elif alfa == "no":
		for i in capas:
			pdb.gimp_layer_flatten(i)
	if colorAlfa == True:
		if modo != 0:
			filtro = "color a alfa"
			error(filtro, tipo, imagen)
		else:
			for i in capas:
				pdb.plug_in_colortoalpha(imagen, i, color)

	if semi == True:
		if modo != 0:
			filtro = "semiaplanar"
			error(filtro, tipo, imagen)
		else:
			for i in capas:
				pdb.plug_in_semiflatten(imagen, i)

	if umbral == True:
		if modo == 2:
			filtro = "umbral a alfa"
			error(filtro, tipo, imagen)
		else:
			for i in capas:
				canalAlfa = pdb.gimp_drawable_has_alpha(i)
				if canalAlfa == True:
					pdb.plug_in_threshold_alpha(imagen, i, nivelUmbral)
	if limpiar == 1 or limpiar == 2:
		for i in capas:
			pdb. gimp_image_select_color(imagen, 2, i, colorBorrar)
			if difuminar == True:
				pdb.gimp_selection_feather(imagen, radio)
			if limpiar == 1:
				pdb.gimp_edit_clear (i)
			else:
				pdb.gimp_edit_bucket_fill(i, 0, 0, 100, 0, False, 0, 0)
	if limpiar == 3:
		for i in capas:
			pdb.gimp_image_select_item(imagen, 2, i)
			pdb.gimp_selection_invert(imagen)
			pdb.gimp_edit_bucket_fill(i, 0, 0, 100, 0, False, 0, 0)

	pdb.gimp_selection_none(imagen)
	pdb.gimp_context_pop()
	pdb.gimp_image_undo_group_end(imagen)


register(
	"python_fu_capas",
	"Varias opciones de transparencias de capa, sobre multiples capas.",
	"añadir o eliminar canal alfa, color a alfa, semiaplanar, umbral alfa y eliminar o remplazar un color, en todas las capas de la imagen.",
	"Jose Antonio Carrascosa García",
	"https://myc-git.gitlab.io/mycweb/",
	"2017",
	"Utilidades de transparencia.",
	"RGB, RGB*, GRAY*, INDEXED, INDEXED*",
	[
		(PF_IMAGE, "imagen", "Animación de fondo.", None),
		(PF_RADIO, "alfa", "Añadir o eliminar canal alfa.", "nada", (("Añadir", "si"), ("Eliminar", "no"),("No hacer nada", "nada"))),
		(PF_BOOL, "colorAlfa", "Color a alfa", False),
		(PF_COLOR, "color", "Color a alfa", (1.0, 1.0, 1.0)),
		(PF_BOOL, "semi", 'Semiaplanar (se utiliza el "Color nuevo")', False),
		(PF_BOOL, "umbral", "Umbral alfa.", False),
		(PF_SLIDER, "nivelUmbral",  "Umbral", 127, (0, 255, 1)),
		(PF_OPTION, "limpiar", "Limpiar color o sustituir", 0, ("No hacer nada", "Limpiar", "Sustituir", "Rellenar tranparente")),
		(PF_BOOL, "difuminar", "Difuminar la selección del color.", True),
		(PF_SPINNER, "radio", "Radio de difuminado", 5, (0, 1000, 1)),
		(PF_COLOR, "colorBorrar", "Color a borrar", (1.0, 1.0, 1.0)),
		(PF_COLOR, "colorRelleno", "Color nuevo.", (0, 0, 0)),
	],
	[],
	inicio,
	menu="<Image>/Filters/Multicapas"
	)

main()
