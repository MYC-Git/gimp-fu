#!/usr/bin/env python
# -*- coding: utf-8 -*-

# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *
from math import pi
import os

#def error(filtro, modo, imagen):
#	pdb.gimp_message("No es posible aplicar " + filtro + " a imágenes del tipo " + modo)

def aguardar(capa, nombre, directorio, altura, anchura, extension):
	imagen_nueva = pdb.gimp_image_new(anchura, altura, 0)
	capa_copia = pdb.gimp_layer_new_from_drawable(capa, imagen_nueva)
	pdb.gimp_image_insert_layer(imagen_nueva, capa_copia, None, 0)
	
	nombreArchivo = directorio + "/" + nombre + "." + extension
	print(nombreArchivo)

	pdb.gimp_file_save(imagen_nueva, capa, nombreArchivo, nombreArchivo)
	pdb.gimp_progress_set_text ("Guardando:" + nombre)
	pdb.gimp_progress_pulse()
	pdb.gimp_image_delete(imagen_nueva)

def listas(opcion = None):
	formatos = [
	["Imágen PNG", "png"], ["Imágen JPEG", "jpg"], ["Imágen GIF", "gif"], ["Imágen de Photoshop", "psd"], ["Imágen PBM", "pbm"], ["Imágen PGM", "pgm"], ["Imágen TarGA", "tga"], ["Imágen TIFF", "tif"], ["Imágen BMP de Windows", "bmp"]
	]

	if opcion == None:
		listaMenu = []
		for i in formatos:
			if i[0] != "":
				listaMenu.append(i[0])
		return listaMenu
	else:
		return formatos[opcion]

def inicio(imagen, aImagen, renombrar, nombre, guardar, formato, directorio):

	pdb.gimp_image_undo_group_start(imagen)
	capas = imagen.layers
	ancho = pdb.gimp_image_width(imagen)
	alto = pdb.gimp_image_height(imagen)

	if renombrar != 0:
		c = len(imagen.layers)
		for i in capas:
			pdb.gimp_item_set_name(i, nombre + str(c))
			c -= 1

	if aImagen == True:
		for i in capas:
			pdb.gimp_layer_resize_to_image_size(i)

	if guardar == True:
		for i in capas:
			nombre = pdb.gimp_item_get_name(i)
			extension = listas(formato)
			extension = extension[1]
			altura = pdb.gimp_image_height(imagen)
			anchura = pdb.gimp_image_width(imagen)
			aguardar(i, nombre, directorio, altura, anchura, extension)

	pdb.gimp_image_undo_group_end(imagen)


register(
	"python_fu_utilidades_varias",
	"Varias opciones sobre multiples capas.",
	"Capas a tamaño de imagen, guardar cada capa como una imagen.",
	"Jose Antonio Carrascosa García",
	"https://myc-git.gitlab.io/mycweb/",
	"2017",
	"Utilidades varias",
	"RGB, RGB*, GRAY, GRAY*, INDEXED, INDEXED*",
	[
		(PF_IMAGE, "imagen", "Animación de fondo.", None),
		(PF_BOOL, "aImagen", "¿Capas a tamaño de imágen?", False),
		(PF_OPTION, "renombrar", "¿Renombrar las capas?", 0,("No", "Si")),
		(PF_STRING, "nombre", "Nombre para las capas:", "The Gimp"),
		(PF_BOOL, "guardar", "Guardar imágen por capa:", False),
		(PF_OPTION, "formato", ("Tipo de archivo"), 0,listas()),
		(PF_DIRNAME, "directorio", "Guardar en:", os.getcwd()),
	],
	[],
	inicio,
	menu="<Image>/Filters/Multicapas"
	)

main()
