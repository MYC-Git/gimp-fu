#!/usr/bin/env python
# -*- coding: utf-8 -*-


#Basado en el siguiente tutorial: http://gimpchat.com/viewtopic.php?f=23&t=7530

# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *
from random import randint

def lunar(in_texto, in_fuente, in_fuentetam, in_cielo):
	ancho = 10
	alto = 10
	
	imagen = pdb.gimp_image_new(ancho, alto, RGB)
	pdb.gimp_image_undo_disable (imagen)

	capa_texto = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Texto", 100, NORMAL_MODE)
	capa_ruido = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Fondo", 100, NORMAL_MODE)
	capa_nubes = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Nubes", 100, GRAIN_MERGE_MODE)
	capa_crater = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Crater", 100, NORMAL_MODE)
	capa_relieve = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Relieve", 100, NORMAL_MODE)

	pdb.gimp_context_push()
	#insertando las capas
	
	pdb.gimp_image_insert_layer(imagen, capa_texto, None, 1)
	pdb.gimp_image_insert_layer(imagen, capa_relieve, None, 0)
	pdb.gimp_image_insert_layer(imagen, capa_nubes, None, 2)
	pdb.gimp_image_insert_layer(imagen, capa_ruido, None, 3)
	pdb.gimp_image_insert_layer(imagen, capa_crater, None, 4)
	
	pdb.gimp_context_set_background ((255, 255, 255))
	pdb.gimp_context_set_foreground ((0, 0, 0))
	pdb.gimp_drawable_fill (capa_texto, TRANSPARENT_FILL)
	el_texto = pdb.gimp_text_fontname(imagen, capa_texto, 0, 0, in_texto, 0, TRUE, in_fuentetam, PIXELS, in_fuente)
	pdb.gimp_text_layer_set_letter_spacing (el_texto, 12)
	ancho = pdb.gimp_drawable_width(el_texto)
	alto = pdb.gimp_drawable_height(el_texto)
	el_bufer = alto * 0.5
	ancho = ancho + el_bufer + el_bufer
	alto = alto + el_bufer + el_bufer
	
	#Escalando
	pdb.gimp_image_resize (imagen, ancho, alto, 0, 0)
	pdb.gimp_layer_set_offsets (el_texto, el_bufer, el_bufer)
	pdb.gimp_layer_resize(capa_texto, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_ruido, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_nubes, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_crater, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_relieve, ancho, alto, 0, 0)
	
	pdb.gimp_floating_sel_anchor (el_texto)
	pdb.gimp_drawable_fill (capa_crater, FOREGROUND_FILL)
	pdb.gimp_context_set_foreground ((128, 128, 128))
	pdb.gimp_drawable_fill (capa_relieve, FOREGROUND_FILL)
	pdb.gimp_drawable_fill (capa_ruido, FOREGROUND_FILL)
	pdb.gimp_drawable_fill (capa_nubes, TRANSPARENT_FILL)
	pdb.gimp_context_set_foreground ((0, 0, 0))
	
	#Aplicando filtros
	pdb.plug_in_hsv_noise (imagen, capa_ruido, 8, 180, 255, 255)
	pdb.gimp_desaturate_full (capa_ruido, 1)
	pdb.plug_in_solid_noise (imagen, capa_nubes, False, False, 0, 1, 16, 16)
	capa_curro = pdb.gimp_image_merge_down (imagen, capa_nubes, 0)
	
	coordenadas = []
	i = 0
	while i < 70:
		x = randint(0, ancho)
		y = randint(0, alto)
		coordenadas.append (x)
		coordenadas.append (y)
		i = i + 1
	pdb.gimp_context_set_brush ("Sparks")
	pdb.gimp_context_set_brush_size (in_fuentetam/5)
	pdb.gimp_paintbrush (capa_crater, 0, 70, coordenadas, 0, 0)
	pdb.gimp_desaturate_full (capa_crater, 1)
	pdb.gimp_invert (capa_crater)
	pdb.plug_in_bump_map (imagen, capa_relieve, capa_curro, 135, 24.51, 4, 0, 0, 0, 0, True, False, 0)
	pdb.plug_in_bump_map (imagen, capa_relieve, capa_crater, 135, 24.51, 4, 0, 0, 0, 0, True, False, 0)
	pdb.gimp_image_remove_layer (imagen, capa_crater)
	pdb.gimp_image_select_item (imagen, 2, capa_texto)
	pdb.plug_in_gauss (imagen, capa_texto, 12.9, 12.9, 0)
	pdb.gimp_context_set_foreground ((128, 128, 128))
	pdb.gimp_edit_fill (capa_texto, 0)
	pdb.gimp_layer_set_mode (capa_relieve, 21)
	capa_final = pdb.gimp_image_merge_down (imagen, capa_relieve, 0)
	pdb.gimp_context_set_foreground ((0, 0, 0))
	pdb.gimp_image_select_item (imagen, 2, capa_final)
	pdb.gimp_edit_fill (capa_curro, BACKGROUND_FILL)
	pdb.gimp_selection_invert (imagen)
	pdb.gimp_edit_fill (capa_curro, FOREGROUND_FILL)
	pdb.gimp_selection_none (imagen)
	pdb.plug_in_gauss (imagen, capa_curro, 20, 20, 0)
	pdb.plug_in_bump_map (imagen, capa_final, capa_curro, 135, 24.51, 8, 0, 0, 0, 0, True, False, 0)
	pdb.gimp_edit_fill (capa_curro, FOREGROUND_FILL)
	
	if in_cielo == True:
		pdb.plug_in_rgb_noise (imagen, capa_curro, 0, 0, 0.20, 020, 0.20, 0)
		pdb.plug_in_sparkle (imagen, capa_curro, 0.001, 0.50, 20, 4, 15, 1, 0, 0, 0, False, False, False, 2)
		pdb.plug_in_sparkle (imagen, capa_curro, 0.081, 0.50, 20, 4, 15, 1, 0, 0, 0, False, False, False, 2)
		pdb.gimp_levels (capa_curro, 0, 50, 225, 1, 0, 255)
		pdb.gimp_item_set_name (capa_curro, "Cielo estrellado")

	#pdb.gimp_selection_none (imagen)#Deseleccionar todo	
	pdb.gimp_image_undo_enable (imagen)
	pdb.gimp_display_new (imagen)
	pdb.gimp_image_clean_all (imagen)
	pdb.gimp_context_pop()


register(
	"python_fu_lunar",
	"Crea un texto con textura lunar.",
	"Creando un texto simulando una textura lunar y un cielo estrellado opcional.",
	"Jose Antonio Carrascosa García, https://myc-git.gitlab.io/mycweb/",
	"Jose Antonio Carrascosa Garcia.",
	"2016",
	"Texto lunar",
	"",
	[
		(PF_STRING, "in_texto", "Texto", "TEXTO LUNAR"),
		(PF_FONT, "in_fuente", "Fuente", "Arial Black,"),
		(PF_SPINNER, "in_fuentetam", "Tamaño del texto", 160, (1, 1000, 10)),
		(PF_BOOL, "in_cielo", "Cielo estrellado.", True),
	],
	[],
	lunar,
	menu="<Image>/File/Create/Python-Fu"
	)

main()

