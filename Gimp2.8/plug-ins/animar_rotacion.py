#!/usr/bin/env python
# -*- coding: utf-8 -*-

# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *
from math import pi
def fondo(imagenNueva, capas, relleno):
	for i in capas:
		pdb.gimp_image_select_item(imagenNueva, 2, i)
		pdb.gimp_selection_invert(imagenNueva)
		pdb.gimp_edit_fill(i, relleno)
		pdb.gimp_selection_none (imagenNueva)
def escalar(capas, modoZoom, an, alt, fotogramas, origen):
	anZ = an / fotogramas
	altZ = alt / fotogramas
	if modoZoom == 1:
		an = 1
		alt = 1
	for i in capas:
		pdb.gimp_layer_scale(i, an, alt, origen)
		if modoZoom == 1:
			an += anZ
			alt += altZ
		else:
			an -= anZ
			if an < 1:
				an = 1
			alt -= altZ
			if alt < 1:
				alt = 1
def rotar(capas, fotogramas, centro, reves, ejeX, ejeY, ancho, alto):
		grados = 360 / fotogramas
		angulo = grados * pi / 180
		if centro == 1:
			X = ejeX
			Y = ejeY
		else:
			X = ancho / 2
			Y = alto / 2
		if reves == False:
			giro = 360 * pi / 180
		else:
			giro = 0
		for i in capas:
			pdb.gimp_item_transform_rotate(i, giro, False, X, Y )
			if reves == False:
				giro = giro - angulo
			else:
				giro = angulo + giro
def redi(imagenNueva, capas):
	pdb.gimp_image_resize_to_layers(imagenNueva)
	for i in capas:
		pdb.gimp_layer_resize_to_image_size(i)
def inicio(imagen, capa, fotogramas, velocidad, modo, redimensionar, zoom, modoZoom, centro, ejeX, ejeY, trans, colorFondo, reves):	
	ancho = pdb.gimp_image_width(imagen)
	alto = pdb.gimp_image_height(imagen)
	imagenNueva = pdb.gimp_image_new(ancho, alto, RGB)
	pdb.gimp_image_undo_disable (imagenNueva)	
	pdb.gimp_context_push()
	pdb.gimp_context_set_background (colorFondo)
	relleno = 5
	capaPrimera = pdb.gimp_layer_new_from_drawable(capa, imagenNueva)
	pdb.gimp_image_insert_layer(imagenNueva, capaPrimera, None, 0)
	if trans == False:
		relleno = 1
	if modo == 0:
		m = "combine"
	else:
		m = "replace"
	origen = True
	if centro == 2:
		origen = False
	indice = 1
	while indice < fotogramas:
		capaNueva = pdb.gimp_layer_new_from_drawable(capaPrimera, imagenNueva)
		pdb.gimp_image_insert_layer(imagenNueva, capaNueva, None, 0)
		indice = indice + 1
	capas = imagenNueva.layers
	if zoom == True:
		escalar(capas, modoZoom, ancho, alto, fotogramas, origen)
	rotar(capas, fotogramas, centro, reves, ejeX, ejeY, ancho, alto)
	if redimensionar == True:
		redi(imagenNueva, capas)
	if reves == True:
		pdb.gimp_image_lower_item_to_bottom(imagenNueva, capaPrimera)
	fondo(imagenNueva, capas, relleno)
	c = len(capas)
	v = int(velocidad)
	v = str(v)
	for i in capas:
		pdb.gimp_item_set_name(i, "Cuadro" + " " + str(c) + " (" + v + "ms) " + " (" + m + ")")
		c = c - 1
	pdb.gimp_display_new (imagenNueva)
	pdb.gimp_image_undo_enable (imagenNueva)
	pdb.gimp_image_clean_all (imagenNueva)
	pdb.gimp_context_pop()
register(
	"python_fu_animar_rotarcion",
	"Animar una rotación.",
	"Animar una rotación con zoom opcional.",
	"Jose Antonio Carrascosa García",
	"https://myc-git.gitlab.io/mycweb/",
	"2017",
	"Rotar",
	"*",
	[
		(PF_IMAGE, "imagen", "Animación de fondo.", None),
		(PF_DRAWABLE, "capa", "Drawable", None),
		(PF_SPINNER, "fotogramas",  "Nº de fotogramas", 0, (10, 360, 1)),
		(PF_SPINNER, "velocidad",  "Velocidad", 100, (0, 65000, 1)),
		(PF_OPTION, "modo", "Modo de transición de las capas:", 1, ("Combinar", "Reemplazar")),
		(PF_BOOL, "redimensionar", "Redimensionar imagen", True),
		(PF_BOOL, "zoom", "Zoom", False),
		(PF_OPTION, "modoZoom", "Acercar o alejar", 0, ("Acercar", "Alejar")),
		(PF_OPTION, "centro", "Eje de rotación", 0, ("Centro", "Eje dado", "Espiral")),
		(PF_SPINNER, "ejeX", "Eje de rotación x", 0, (0, 8000, 1)),
		(PF_SPINNER, "ejeY", "Eje de rotación y", 0, (0, 8000, 1)),
		(PF_BOOL, "trans", "Fondo transparente", True),
		(PF_COLOR, "colorFondo", "Color de fondo", (0,0,0)),
		(PF_BOOL, "reves", "Sentido antihorario", False),
	],
	[],
	inicio,
	menu="<Image>/Filters/Animation"
	)

main()


