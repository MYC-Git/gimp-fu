#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Script inspirado en el tutorial: http://mirincon-misaani.blogspot.com.es/2011/02/el-gimp-textura-papel-arrugado.html y
#http://mirincon-misaani.blogspot.com.es/2011/03/el-gimp-papel-viejo.html


# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *
from random import randint

	
def arrugado(imagen, capa, envejecer):


	#Preparando el terreno
	pdb.gimp_context_push
	pdb.gimp_image_undo_group_start (imagen)
	pdb.gimp_context_set_background ((255, 255, 255))
	pdb.gimp_context_set_foreground ((0, 0, 0))
	
	pdb.gimp_context_set_background ((255, 255, 255))
	pdb.gimp_context_set_foreground ((000, 000, 000))
	modo = pdb.gimp_image_base_type (imagen)
	ancho = pdb.gimp_image_width (imagen)
	alto = pdb.gimp_image_height (imagen)
	
	
	capa_fondo = pdb.gimp_layer_new (imagen, ancho, alto, modo, "Arrugado", 100, 0)
	pdb.gimp_image_insert_layer (imagen, capa_fondo, None, 1)
	pdb.gimp_image_lower_item_to_bottom (imagen, capa_fondo)
	pdb.gimp_layer_add_alpha (capa)
	pdb.gimp_layer_add_alpha (capa_fondo)
	i = 0
	while i<7:
		x = randint (0, ancho)
		y = randint (0, alto)
		x1 = randint (0, ancho)
		y1 = randint (0, alto)
		x2 = randint (0, ancho)
		y2 = randint (0, alto)
		x3 = randint (0, ancho)
		y3 = randint (0, alto)
		pdb.gimp_edit_blend (capa_fondo, FG_BG_RGB_MODE, DIFFERENCE_MODE, GRADIENT_BILINEAR, 100, 0, REPEAT_NONE, FALSE, FALSE, 0, 0, TRUE, x, y, x1, y1)
		pdb.gimp_edit_blend (capa_fondo, FG_BG_RGB_MODE, DIFFERENCE_MODE, GRADIENT_RADIAL, 100, 0, REPEAT_NONE, FALSE, FALSE, 0, 0, TRUE, x2, y2, x3, y3)
		i = i + 1
	
	pdb.plug_in_emboss (imagen, capa_fondo, 10, 45, 6, 1)#repujado
	pdb.plug_in_gauss (imagen, capa_fondo, 10, 10, 0)
	pdb.plug_in_displace (imagen, capa, 20, 20, 20, 20, capa_fondo, capa_fondo, 3)
	pdb.plug_in_displace (imagen, capa, 10, 10, 10, 10, capa_fondo, capa_fondo, 3)
	
	
	
	#if modo
	if modo == GRAY:
		modo = GRAYA
		modo = RGBA
	#if envejecer
	if envejecer == True:
		pdb.gimp_selection_all (imagen)
		pdb.gimp_selection_shrink (imagen, 20)
		capa_curro = pdb.gimp_layer_new (imagen, ancho, alto, modo, "Fondo", 100, 0)
		pdb.gimp_image_insert_layer (imagen, capa_curro, None, 2)
		pdb.gimp_image_lower_item_to_bottom (imagen, capa_curro)
		pdb.gimp_edit_fill (capa_curro, BACKGROUND_FILL)
		pdb.gimp_selection_invert (imagen)
		pdb.gimp_edit_clear (capa_curro)
		pdb.gimp_selection_invert (imagen)
		pdb.gimp_selection_none (imagen)
		pdb.gimp_layer_scale (capa_curro, (ancho/4), (alto/4), True)
		pdb.plug_in_spread (imagen, capa_curro, 8, 8)
		pdb.plug_in_gauss_iir (imagen, capa_curro, 1, True, True)
		pdb.gimp_layer_scale (capa_curro, ancho, alto, True)
		pdb.gimp_layer_add_alpha (capa_curro)
		pdb.plug_in_threshold_alpha (imagen, capa_curro, 127)
		pdb.plug_in_gauss_iir (imagen, capa_curro, 1, True, True)
		pdb.gimp_image_select_item (imagen, 2, capa_curro)
		if pdb.gimp_item_is_channel (capa) == True and pdb.gimp_item_is_layer_mask (capa) == False:
			pdb.gimp_image_set_active_channel (imagen, capa)
		#pdb.gimp_selection_all (imagen)
		pdb.gimp_selection_shrink (imagen, 20)
		pdb.gimp_selection_feather (imagen, 15)
		pdb.gimp_selection_invert (imagen)
		pdb.gimp_edit_clear (capa_fondo)
		pdb.gimp_edit_clear (capa)
		pdb.gimp_selection_none (imagen)
		pdb.gimp_edit_fill (capa_curro, BACKGROUND_FILL)
		pdb.plug_in_hsv_noise (imagen, capa, 2, 180, 0, 30)
		pdb.plug_in_hsv_noise (imagen, capa_fondo, 2, 180, 0, 15)
	pdb.gimp_layer_set_opacity (capa, 50)

	

#	pdb.gimp_selection_none (imagen)#Deseleccionar todo	
	pdb.gimp_image_undo_group_end (imagen)
	pdb.gimp_displays_flush (imagen)
	#pdb.gimp_image_clean_all (imagen)
	pdb.gimp_context_pop


register(
	"python_fu_imagen_arrugada",
	"Simula el efecto de un papel arrugado.",
	"Crea el efecto de una imagen arrugada.",
	"Jose Antonio Carrascosa García, https://myc-git.gitlab.io/mycweb/",
	"Jose Antonio Carrascosa Garcia.",
	"2016",
	"Arrugar...",
	"RGB*",
	[
		(PF_IMAGE, "imagen", "Imagen", None),
		(PF_DRAWABLE, "drawable", "Drawable", None),
		(PF_BOOL, "envejecer", "Envejecer", True),
#		(PF_COLOR, "in_colorcristal", "Color del cristal", (255, 255, 255)),

	],
	[],
	arrugado,
	menu="<Image>/Filters/Decor"
	)

main()

