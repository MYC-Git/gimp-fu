#!/usr/bin/env python
# -*- coding: utf-8 -*-

#basado en el siguiente tutorial: http://gimpchat.com/viewtopic.php?f=23&t=6652
# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *

def pelo(imagen, capa):
	capa_copia = pdb.gimp_layer_copy(capa, True)
	pdb.gimp_image_insert_layer (imagen, capa_copia, None, 2)

	pdb.plug_in_gauss(imagen, capa_copia, 40, 40, 1)
	pdb.plug_in_hsv_noise(imagen, capa_copia, 2, 3, 10, 12)
	pdb.plug_in_bump_map(imagen,capa, capa_copia, 120, 45, 65, 0, 0, 0, 0, True, False, 0)
	pdb.gimp_image_remove_layer(imagen, capa_copia)

def mapa(imagen, capa):
	pdb.gimp_image_select_item(imagen, 2, capa)
	pdb.plug_in_solid_noise (imagen, capa, 1, 0, 0, 1, 2.8, 2.8)#Ruido sólido
	pdb.plug_in_gradmap (imagen, capa)#Mapa de degradado
	pdb.gimp_selection_none (imagen)
def texto(in_texto, in_fuente, in_fuentetam, in_colortexto, degradado, in_degra, fondo, in_colorfondo, degradadof, in_degraf, in_colorsombra):
	ancho = 10
	alto = 10
	
	imagen = pdb.gimp_image_new(ancho, alto, RGB)
	pdb.gimp_image_undo_disable (imagen)
	capa_texto = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Texto", 100, NORMAL_MODE)
	capa_fondo = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Fondo", 100, NORMAL_MODE)

	pdb.gimp_image_insert_layer(imagen, capa_texto, None, 0)
	pdb.gimp_context_push()
	pdb.gimp_context_set_background (in_colorfondo)
	pdb.gimp_context_set_foreground (in_colortexto)
	pdb.gimp_context_set_brush ("Confetti")
	
	texto = pdb.gimp_text_fontname(imagen, capa_texto, 0, 0, in_texto, 0, TRUE, in_fuentetam, PIXELS, in_fuente)
	ancho = pdb.gimp_drawable_width(texto)
	alto = pdb.gimp_drawable_height(texto)
	margen = alto * 0.5
	ancho = ancho + margen + margen
	alto = alto + margen + margen
	
	pdb.gimp_image_resize (imagen, ancho, alto, 0, 0)
	pdb.gimp_layer_set_offsets (texto, margen, margen)
	pdb.gimp_layer_resize(capa_texto, ancho, alto, 0, 0,)
	pdb.gimp_image_insert_layer (imagen, capa_fondo, None, 2)
	pdb.gimp_layer_resize(capa_fondo, ancho, alto, 0, 0,)
	
	pdb.gimp_drawable_fill (capa_texto, TRANSPARENT_FILL)
	pdb.gimp_drawable_fill (capa_fondo, BACKGROUND_FILL)

	pdb.gimp_floating_sel_anchor (texto)
	if degradado == True:
		pdb.gimp_context_set_gradient (in_degra)#Degradado
		mapa(imagen, capa_texto)
	pelo(imagen, capa_texto)
	if degradadof == True:
		pdb.gimp_context_set_gradient (in_degraf)#Degradado
		mapa(imagen, capa_fondo)
	if fondo == True:
		pelo(imagen, capa_fondo)
	
	pdb.gimp_image_select_item(imagen, 2, capa_texto)
	pdb.script_fu_distress_selection(imagen, capa_texto, 180, 14, 1, 3, True, True)
	pdb.gimp_selection_invert(imagen)
	pdb.gimp_edit_clear(capa_texto)
	pdb.gimp_selection_none (imagen)
	pdb.script_fu_drop_shadow(imagen, capa_texto, 4, 4, 15, in_colorsombra, 100, False)
		
	pdb.gimp_selection_none (imagen)	
	pdb.gimp_image_undo_enable (imagen)
	pdb.gimp_display_new (imagen)
	pdb.gimp_image_clean_all (imagen)
	pdb.gimp_context_pop()


register(
	"python_fu_texto_peludo",
	"Texto peludo.",
	"Texto peludo.",
	"Jose Antonio Carrascosa García, https://myc-git.gitlab.io/mycweb/",
	"Jose Antonio Carrascosa Garcia.",
	"2018",
	"Texto peludo",
	"",
	[
		(PF_STRING, "in_texto", "Texto", "GIMP"),
		(PF_FONT, "in_fuente", "Fuente", "Arial Black"),
		(PF_SPINNER, "in_fuentetam", "Tamaño del texto", 120, (1, 300, 1)),
		(PF_COLOR, "in_colortexto", "Color del texto", (109, 109, 224)),
		(PF_BOOL, "degradado", "Usar mapa de degradado", False),
		(PF_GRADIENT, "in_degra", "Degradado", "Golden"),
		(PF_BOOL, "fondo", "Fondo peludo", False),
		(PF_COLOR, "in_colorfondo", "Color del fondo", (255,255,255)),
		(PF_BOOL, "degradadof", "Usar mapa de degradado en el fondo", False),
		(PF_GRADIENT, "in_degraf", "Degradado para el fondo", "Golden"),
		(PF_COLOR, "in_colorsombra", "Color de la sombra", (0,0,0)),
		
	],
	[],
	texto,
	menu="<Image>/File/Create/Python-Fu"
	)

main()

