#!/usr/bin/env python
# -*- coding: utf-8 -*-

# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *
from math import floor, ceil
import os

def util_anima(imagenFondo, renombrar, velocidad, nombre, modo, invertir):

	if renombrar == True or invertir == True:
		imagenBase = pdb.gimp_image_duplicate(imagenFondo)
		capas = imagenBase.layers
		if renombrar == True:
			d = len(capas)
			for i in capas:
				if modo == 0:
					m = "combine"
				else:
					m = "replace"
				pdb.gimp_item_set_name(i, nombre + " " + str(d) + " (" + velocidad + "ms) " + " (" + m + ")")
				d -= 1
		if invertir == True:
			pdb.script_fu_reverse_layers(imagenBase, capas[0])
		pdb.gimp_selection_none(imagenBase)
		pdb.gimp_display_new (imagenBase)

register(
	"python_fu_utilidades_animacion",
	"Varias operaciones sobre múltiples capas",
	"Conjunto de operaciones sobre todas las capas de la imagen que pueden resultar útiles para crear o modificar animaciones, como: renombrar las capas o invertir el orden de todas las capas de la imagen.",
	"Jose Antonio Carrascosa García",
	"https://myc-git.gitlab.io/mycweb/",
	"2017",
	"Utilidades de animación",
	"RGB, RGB*, GRAY*, INDEXED, INDEXED*",
	[
		(PF_IMAGE, "imagenFondo", "Animación de fondo.", None),
		(PF_BOOL, "renombrar", "¿Renombrar las capas?", False),
		(PF_STRING, "velocidad", "Velocidad de los fotogramas (ms):", "100"),
		(PF_STRING, "nombre", "Nombre para las capas:", "The Gimp"),
		(PF_OPTION, "modo", "Modo de transición de las capas:", 0, ("Combinar", "Reemplazar")),
		(PF_BOOL, "invertir", "¿Invertir el orden de las capas?", False),

	],
	[],
	util_anima,
	menu="<Image>/Filters/Multicapas"
	)

main()
