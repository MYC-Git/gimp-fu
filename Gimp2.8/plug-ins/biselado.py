#!/usr/bin/env python
# -*- coding: utf-8 -*-

#basado en el siguiente tutorial: http://gimpchat.com/viewtopic.php?f=10&t=6268
# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gimpfu import *

def bisel(in_texto, in_fuente, in_fuentetam, in_colortexto, in_colorfondo):
	ancho = 10
	alto = 10
	
	imagen = pdb.gimp_image_new(ancho, alto, RGB)
	pdb.gimp_image_undo_disable (imagen)
	capa_bisel = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Bisel", 100, NORMAL_MODE)
	capa_texto = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Texto", 100, NORMAL_MODE)
	capa_fondo = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Fondo", 100, NORMAL_MODE)

	pdb.gimp_image_insert_layer(imagen, capa_texto, None, 0)
	pdb.gimp_context_push()
	pdb.gimp_context_set_background (in_colorfondo)
	pdb.gimp_context_set_foreground (in_colortexto)
	
	texto = pdb.gimp_text_fontname(imagen, capa_texto, 0, 0, in_texto, 0, TRUE, in_fuentetam, PIXELS, in_fuente)
	ancho = pdb.gimp_drawable_width(texto)
	alto = pdb.gimp_drawable_height(texto)
	margen = alto * 0.5
	ancho = ancho + margen + margen
	alto = alto + margen + margen
	
	pdb.gimp_image_resize (imagen, ancho, alto, 0, 0)
	pdb.gimp_layer_set_offsets (texto, margen, margen)
	pdb.gimp_layer_resize(capa_texto, ancho, alto, 0, 0,)
	pdb.gimp_image_insert_layer (imagen, capa_fondo, None, 2)
	pdb.gimp_layer_resize(capa_fondo, ancho, alto, 0, 0,)
	pdb.gimp_image_insert_layer (imagen, capa_bisel, None, 0)
	pdb.gimp_layer_resize(capa_bisel, ancho, alto, 0, 0,)

	pdb.gimp_drawable_fill (capa_texto, TRANSPARENT_FILL)
	pdb.gimp_context_set_background ((0, 0, 0))
	pdb.gimp_drawable_fill (capa_bisel, BACKGROUND_FILL)
	pdb.gimp_context_set_background (in_colorfondo)
	pdb.gimp_drawable_fill (capa_fondo, BACKGROUND_FILL)
	pdb.gimp_floating_sel_anchor (texto)

	#bisel
	pdb.gimp_image_select_item(imagen, 2, capa_texto)
	pdb.gimp_selection_feather(imagen, 15)
	pdb.gimp_context_set_background ((255, 255, 255))
	pdb.gimp_edit_fill (capa_bisel, BACKGROUND_FILL)
	pdb.gimp_selection_none (imagen)
	pdb.plug_in_emboss(imagen, capa_bisel, 30, 45, 10, 0)
	pdb.gimp_image_select_item(imagen, 2, capa_texto)
	mascara = pdb.gimp_layer_create_mask(capa_bisel, ADD_SELECTION_MASK)
	pdb.gimp_layer_add_mask(capa_bisel, mascara)
	pdb.gimp_selection_none (imagen)
	pdb.plug_in_colortoalpha(imagen, capa_bisel, (180, 180, 180))
	
	
	
	
	
	lista = pdb.gimp_gradients_get_list("")
	print(lista)
	a = lista[0]
	i = 0
	while i < a:
		print(lista[1][i])
		i = i+1
	
	
	
	
	pdb.gimp_image_undo_enable (imagen)
	pdb.gimp_display_new (imagen)
	pdb.gimp_image_clean_all (imagen)
	pdb.gimp_context_pop()


register(
	"python_fu_texto_biselado",
	"Texto biselado.",
	"Texto biselado.",
	"Jose Antonio Carrascosa García, https://myc-git.gitlab.io/mycweb/",
	"Jose Antonio Carrascosa Garcia.",
	"2018",
	"Texto biselado",
	"",
	[
		(PF_STRING, "in_texto", "Texto", "GIMP"),
		(PF_FONT, "in_fuente", "Fuente", "Arial Black"),
		(PF_SPINNER, "in_fuentetam", "Tamaño del texto", 120, (1, 300, 1)),
		(PF_COLOR, "in_colortexto", "Color del texto", (255, 0, 0)),
		(PF_COLOR, "in_colorfondo", "Color del fondo", (255,255,255)),
		
	],
	[],
	bisel,
	menu="<Image>/File/Create/Python-Fu"
	)

main()

