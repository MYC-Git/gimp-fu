#!/usr/bin/env python
# -*- coding: utf-8 -*-


# GIMP - The GNU Image Manipulation Program
# Copyright (C) 1995 Spencer Kimball and Peter Mattis
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Basado en este tutorial: https://narodny-geroy.deviantart.com/art/Tutorial-Fireballs-with-Gimp-153793175



from gimpfu import *
from random import randint

def fuego(in_texto, in_fuente, in_fuentetam, in_colorfondo, in_sombra, in_colorsombra, distorsion):
	ancho = 10
	alto = 10
	imagen = pdb.gimp_image_new(ancho, alto, RGB)
	pdb.gimp_image_undo_disable (imagen)

	capa_texto = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Fondo", 100, NORMAL_MODE)
	capa_desp = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Desplazamiento", 100, HARDLIGHT_MODE)
	capa_fuego = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Fuego", 100, NORMAL_MODE)
	capa_quemar = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Quemando", 100, BURN_MODE)
	pdb.gimp_context_push()
	#insertando las capas
	pdb.gimp_image_insert_layer(imagen, capa_texto, None, 0)
	pdb.gimp_image_insert_layer(imagen, capa_fuego, None, 0)
	pdb.gimp_image_insert_layer(imagen, capa_desp, None, 0)
	pdb.gimp_image_insert_layer(imagen, capa_quemar, None, 0)
	
	pdb.gimp_context_set_background ((255, 255, 255))
	pdb.gimp_context_set_foreground ((0, 0, 0))
	
	el_texto = pdb.gimp_text_fontname(imagen, capa_texto, 0, 0, in_texto, 0, TRUE, in_fuentetam, PIXELS, in_fuente)
	ancho = pdb.gimp_drawable_width(el_texto)
	alto = pdb.gimp_drawable_height(el_texto)
	el_bufer = alto * 0.5
	ancho = ancho + el_bufer + el_bufer
	alto = alto + el_bufer + el_bufer
	#Escalando
	pdb.gimp_image_resize (imagen, ancho, alto, 0, 0)
	pdb.gimp_layer_set_offsets (el_texto, el_bufer, el_bufer)
	pdb.gimp_layer_resize(capa_texto, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_fuego, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_desp, ancho, alto, 0, 0)
	pdb.gimp_layer_resize(capa_quemar, ancho, alto, 0, 0)
	pdb.gimp_floating_sel_anchor (el_texto)
	
	#Al meollo de la cuestión
	#1907153976
	r = randint(0, 4294967295)
	print(r)
	pdb.gimp_drawable_fill (capa_fuego, FOREGROUND_FILL)
	pdb.gimp_edit_blend (capa_quemar, 0, 0, GRADIENT_LINEAR, 100, 0, REPEAT_NONE, False, False, 0, 0, False, ancho/2, 0, ancho/2, alto - (alto/2))
	pdb.plug_in_solid_noise (imagen, capa_fuego, False, True, r, 15, 9.0, 7.0 )
	pdb.plug_in_solid_noise (imagen, capa_desp, False, False, 0, 15, 11.0, 9.0 )

	pdb.plug_in_displace (imagen, capa_fuego, 0, 30, True, True, capa_desp, capa_desp, 2)

	pdb.gimp_image_merge_down(imagen, capa_desp, 0)
	capa_fuego = pdb.gimp_image_merge_down(imagen, capa_quemar, 0)
	pdb.gimp_color_balance (capa_fuego, 0, True, 100, 0, -100)
	pdb.gimp_color_balance (capa_fuego, 1, True, 100, 0, -100)
	pdb.gimp_brightness_contrast(capa_fuego, 50, 40)

	pdb.gimp_image_select_item (imagen, 2, capa_texto)
	if distorsion == True:
		pdb.script_fu_distress_selection( imagen, capa_texto, 127, 8, 2, 2, True, True)
	pdb.gimp_selection_invert (imagen)
	pdb.gimp_edit_clear (capa_fuego)

	if in_sombra == True:
		capa_sombra = pdb.gimp_layer_new(imagen, ancho, alto, RGBA_IMAGE, "Sombra", 100, NORMAL_MODE)
		pdb.gimp_image_insert_layer (imagen, capa_sombra, None, 1)
		pdb.gimp_selection_invert (imagen)
		if distorsion == False:
			pdb.gimp_image_select_item (imagen, 2, capa_texto)#Alfa a selección
		pdb.gimp_context_set_background (in_colorsombra)
		pdb.gimp_edit_fill (capa_sombra, BACKGROUND_FILL)
		pdb.plug_in_gauss (imagen, capa_sombra, 10, 10, 0)
		pdb.gimp_layer_set_offsets (capa_sombra, 4, 4)
		pdb.gimp_layer_resize_to_image_size (capa_sombra)
		pdb.gimp_layer_set_opacity (capa_sombra, 60)

	pdb.gimp_selection_none (imagen)
	pdb.gimp_context_set_foreground (in_colorfondo)
	pdb.gimp_drawable_fill (capa_texto, FOREGROUND_FILL)
	
	pdb.gimp_image_undo_enable (imagen)
	pdb.gimp_display_new (imagen)
	pdb.gimp_image_clean_all (imagen)

	pdb.gimp_context_pop()


register(
	"python_fu_fuego",
	"Crea el efecto de un texto con llamas en su interior.",
	"Crea el efecto de un texto ardiendo en su interior.",
	"Jose Antonio Carrascosa García, https://myc-git.gitlab.io/mycweb/",
	"Jose Antonio Carrascosa Garcia.",
	"2018",
	"Texto ardiendo",
	"",
	[
		(PF_STRING, "in_texto", "Texto", "THE GIMP"),
		(PF_FONT, "in_fuente", "Fuente", "Arial Black"),
		(PF_SPINNER, "in_fuentetam", "Tamaño del texto", 120, (1, 300, 1)),
		(PF_COLOR, "in_colorfondo", "Color de fondo", (0, 0, 0)),
		(PF_BOOL, "in_sombra", "Añadir sombra.", True),
		(PF_COLOR, "in_colorsombra", "Color de la sombra", (255, 255, 255)),
		(PF_BOOL, "distorsionar", "Distorsionar.", True),
	],
	[],
	fuego,
	menu="<Image>/File/Create/Python-Fu"
	)

main()

