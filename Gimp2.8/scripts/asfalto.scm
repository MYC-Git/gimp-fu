
;***************************************************************************************************************
; La licencia:
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;***************************************************************************************************************

(define (script-fu-texto-asfalto inTexto inFuente inFuenteTam )
(let*
    (
	   ; definir las variables locales
	   ; crear la imagen nueva:
	(elAnchodeImagen  10)
	(laAlturadeImagen 10)
	(laImagen (car
			(gimp-image-new
			 elAnchodeImagen
			 laAlturadeImagen
			 RGB
			)
		    )
	 )
	(elTexto)   ;una declaración para el texto.
	(theBuffer)

	(gimp-image-undo-disable laImagen)
;Crear una capa nueva en la imagen:
	(laCapaTexto
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Capa de texto"
			100
			NORMAL
			)
		)
	  )
	(laCapa
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Capa"
			100
			NORMAL
			)
		)
	  )
	(laCapaFondo
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Asfalto"
			100
			NORMAL
			)
		)
	)
	(laCapaTextura
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"textura"
			100
			NORMAL
			)
		)
	)
	) ; fin de las variables locales
;insertando las capas
(gimp-image-insert-layer laImagen laCapaTexto 0 0)
(gimp-image-insert-layer laImagen laCapa 0 1)
(gimp-image-insert-layer laImagen laCapaFondo 0 2)

(gimp-context-push)
(gimp-context-set-background '(255 255 255) )
(gimp-context-set-foreground '(000 000 000) )

(gimp-drawable-fill laCapaTexto TRANSPARENT-FILL)

	(set! elTexto
		(car
			(gimp-text-fontname
			laImagen laCapaTexto
			0 0
			inTexto
			0
			TRUE
			inFuenteTam PIXELS
			inFuente)
		)
	)

	(set! elAnchodeImagen (car (gimp-drawable-width elTexto) ) )
	(set! laAlturadeImagen (car (gimp-drawable-height elTexto) ) )
	(set! theBuffer(* laAlturadeImagen(/ 35 100) ) )
	(set! laAlturadeImagen(+ laAlturadeImagen theBuffer theBuffer) )
	(set! elAnchodeImagen(+ elAnchodeImagen theBuffer theBuffer) )
	(gimp-image-resize laImagen elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-set-offsets elTexto theBuffer theBuffer)
	(gimp-layer-resize laCapa elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize laCapaTexto elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize laCapaFondo elAnchodeImagen laAlturadeImagen 0 0)
(gimp-drawable-fill laCapaFondo FOREGROUND-FILL)
(gimp-drawable-fill laCapa BACKGROUND-FILL)
(gimp-floating-sel-anchor elTexto)
;Aplicando los filtros
	(plug-in-gauss 1 laImagen laCapaTexto 4 4 0) ;Desenfoque gaussiano al texto
	(plug-in-hsv-noise 1 laImagen laCapaFondo 1 0 0 255);Ruido-hsv a la capa negra
	(plug-in-hsv-noise 1 laImagen laCapa 1 0 0 255);Ruido-hsv a la capa blanca
	(gimp-image-select-item laImagen 2 laCapaTexto);alfa a selección en la capa texto
	(gimp-selection-invert laImagen);invertir la selección
	(gimp-edit-clear laCapa);borra lo seleccionado en la capa blanca
	(gimp-selection-none laImagen);seleccionar nada
	(set! laCapaTextura (car (gimp-layer-copy laCapaFondo TRUE)));Copia la capa de fondo en la capa textura
	(gimp-image-insert-layer laImagen laCapaTextura 0 2);inserta la capa textura en la imagen
	(plug-in-bump-map 1 laImagen laCapaTextura laCapa 135 21 33 0 0 0 0 TRUE FALSE 0);mapa de relieve
	(gimp-image-select-item laImagen 2 laCapaTexto);alfa a selección
	(gimp-selection-grow laImagen 3);agrandar la selección
	(gimp-selection-invert laImagen);invertir la selección
	(gimp-edit-clear laCapaTextura);limpia lo seleccionado
	(gimp-selection-none laImagen);seleccionar nada
	(gimp-image-remove-layer laImagen laCapaTexto);elimina la capa texto
	(gimp-item-set-name laCapaTextura "Relieve");renombrar la capa textura
	(gimp-item-set-visible laCapaTextura 1);hacer visible la capa textura
	(plug-in-gauss 1 laImagen laCapaFondo 1 1 0);desenfoque gaussiano a la capa de fondo
	(gimp-image-set-active-layer laImagen laCapa);marcada como activa la capa de arriba

	
(gimp-image-undo-enable laImagen)
(gimp-display-new laImagen)
(gimp-image-clean-all laImagen)
(gimp-context-pop)
))
	(script-fu-register "script-fu-texto-asfalto"                                   ;nombre de la función 1REQUERIDO
	"Texto asfaltado"                                          ;etiqueta del menú 2REQUERIDO
   	"Crea un texto simulando una textura de asfalto." ;descripción 3REQUERIDO
	"Jose Antonio Carrascosa Garcia"                                                     ;autor 4REQUERIDO
	"Copyright 2014 https://myc-git.gitlab.io/mycweb/index.html"                                      ;copyright 5REQUERIDO
	"Miércoles 8 de octubre de 2014"                         ;Fecha de creación 6REQUERIDO
	""                                                         ;Tipo de imagen con el que trabaja. 7REQUERIDO
	SF-STRING        _"Texto"       "En la autopista..."                  ;Variable de cadena.
	SF-FONT          _"Tipo de fuente"       "Georgia Bold"          ;Variable tipografia.
	SF-ADJUSTMENT    _"Tamaño del texto"  '(120 1 1000 1 10 0 1)
	)
	(script-fu-menu-register  "script-fu-texto-asfalto" "<Image>/File/Create")
