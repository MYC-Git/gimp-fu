;  Script inspirado en el tutorial: http://mirincon-misaani.blogspot.com.es/2011/02/el-gimp-textura-papel-arrugado.html y
;http://mirincon-misaani.blogspot.com.es/2011/03/el-gimp-papel-viejo.html
;***************************************************************************************************************
; La licencia:
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;***************************************************************************************************************

(define (script-fu-Texto-arrugado inTexto inFuente inColorTexto inFuenteTam inColorFondo envejecer)
(let*
    (
	   ; definir las variables locales
	   ; crear la imagen nueva:
	(elAnchodeImagen  10)
	(laAlturadeImagen 10)
	(feather1 (/ inFuenteTam 3))
        (feather2 (/ inFuenteTam 7))
        (feather3 (/ inFuenteTam 10))
	(capaCurro 0)
	(capaDifu 0)

	(laImagen (car
			(gimp-image-new
			 elAnchodeImagen
			 laAlturadeImagen
			 RGB
			)
		    )
	 )
	(elTexto)   ;una declaración para el texto.
	(elBufer)
	(elModo (car (gimp-image-base-type laImagen)))
	(gimp-image-undo-disable laImagen)
;Crear una capa nueva en la imagen:
	(capaTexto
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Texto"
			100
			NORMAL
			)
		)
	  )
	(capaFondo
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Capa 2"
			100
			NORMAL
			)
		)
	  )
	(capaBrillo
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Papel"
			30
			NORMAL
			)
		)
	  )
	) ; fin de las variables locales
(gimp-image-insert-layer laImagen capaTexto 0 0)


(gimp-context-push)
(gimp-context-set-background '(255 255 255) )
(gimp-context-set-foreground inColorTexto )
(gimp-drawable-fill capaTexto TRANSPARENT-FILL)
	(set! elTexto
		(car
			(gimp-text-fontname
			laImagen capaTexto
			0 0
			inTexto
			0
			TRUE
			inFuenteTam PIXELS
			inFuente)
		)
	)
(gimp-context-set-foreground '(0 0 0) )
	(set! elAnchodeImagen (car (gimp-drawable-width elTexto) ) )
	(set! laAlturadeImagen (car (gimp-drawable-height elTexto) ) )
	(set! elBufer(* laAlturadeImagen(/ 50 100) ) )
	(set! laAlturadeImagen(+ laAlturadeImagen elBufer elBufer) )
	(set! elAnchodeImagen(+ elAnchodeImagen elBufer elBufer) )
	(gimp-image-resize laImagen elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-set-offsets elTexto elBufer elBufer)
	(gimp-layer-resize capaTexto elAnchodeImagen laAlturadeImagen 0 0)
	(set! capaFondo (car (gimp-layer-copy capaTexto TRUE)))
	(gimp-image-insert-layer laImagen capaFondo 0 3)
	(gimp-image-insert-layer laImagen capaBrillo 0 2)
	(gimp-layer-resize capaBrillo elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-item-set-name capaFondo "Arrugado")
	(gimp-context-set-background inColorFondo)
	(gimp-drawable-fill capaFondo BACKGROUND-FILL)
	(gimp-drawable-fill capaBrillo BACKGROUND-FILL)
;Anclar el texto y establecer la opacidad de la capa texto
	(gimp-floating-sel-anchor elTexto)
	(gimp-layer-set-opacity capaTexto 50)
;aplicar los degradados
(gimp-context-set-background '(255 255 255) )
(gimp-context-set-foreground '(000 000 000) )
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 2) laAlturadeImagen (/ elAnchodeImagen 2) 0)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 2) 0 (/ elAnchodeImagen 2) laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	elAnchodeImagen (/ laAlturadeImagen 2) (/ elAnchodeImagen 2) laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	0 laAlturadeImagen 0 0)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	0 0 elAnchodeImagen laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	0 0 elAnchodeImagen laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 2) (/ laAlturadeImagen 2) 0 (/ laAlturadeImagen 2))
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 6) (/ laAlturadeImagen 2) elAnchodeImagen laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(* (/ elAnchodeImagen 4) 3) (* (/ laAlturadeImagen 3) 2) 0 0)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(* (/ elAnchodeImagen 4) 3) (* (/ laAlturadeImagen 3) 2) elAnchodeImagen laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 4) (* (/ laAlturadeImagen 3) 2) 0 0)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 4) (* (/ laAlturadeImagen 3) 2) elAnchodeImagen laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 4) (/ laAlturadeImagen 3) elAnchodeImagen laAlturadeImagen)
	(gimp-edit-blend capaFondo FG-BG-RGB-MODE DIFFERENCE-MODE GRADIENT-BILINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 TRUE 
	(/ elAnchodeImagen 4) (* (/ laAlturadeImagen 3) 2) elAnchodeImagen laAlturadeImagen)
;Los plugins
	(plug-in-emboss 1 laImagen capaFondo 10 45 6 1);repujado
	(plug-in-gauss 1 laImagen capaFondo 10 10 0)
	(plug-in-displace 1 laImagen capaTexto 20 20 20 20 capaFondo capaFondo 1);desplazar
(gimp-selection-none laImagen);Deseleccionar todo
	(if (= elModo GRAY);preparando para distorsionar la selección
	  (set! elModo GRAYA-IMAGE)
	  (set! elModo RGBA-IMAGE)
	)


	(if (= envejecer TRUE)
	(begin
	(gimp-selection-all laImagen)
	(gimp-selection-shrink laImagen 20)
	(set! capaCurro (car (gimp-layer-new laImagen elAnchodeImagen laAlturadeImagen elModo "Distorsión" 100 0)));capa nueva para distorsionar la selección
	(set! capaDifu (car (gimp-layer-new laImagen elAnchodeImagen laAlturadeImagen elModo "Fondo" 100 0)));capa nueva de fondo para el difuminado
	(gimp-image-insert-layer laImagen capaCurro 0 0)
	(gimp-image-insert-layer laImagen capaDifu 0 4)
	(gimp-edit-fill capaCurro BACKGROUND-FILL)
	(gimp-selection-invert laImagen)
	(gimp-edit-clear capaCurro)
	(gimp-selection-invert laImagen)
	(gimp-selection-clear laImagen)
	(gimp-layer-scale capaCurro (/ elAnchodeImagen 4) (/ laAlturadeImagen 4) TRUE)
	(plug-in-spread 1 laImagen capaCurro 8 8)
	(plug-in-gauss-iir 1 laImagen capaCurro 1 TRUE TRUE )
	(gimp-layer-scale capaCurro elAnchodeImagen laAlturadeImagen TRUE)
	(plug-in-threshold-alpha 1 laImagen capaCurro 127)
	(plug-in-gauss-iir 1 laImagen capaCurro 1 TRUE TRUE )

	(gimp-image-select-item laImagen 2 capaCurro)
		(if (and (= (car (gimp-item-is-channel capaFondo)) TRUE)
		     (= (car (gimp-item-is-layer-mask capaFondo)) FALSE))
	      (gimp-image-set-active-channel theImage capaFondo)
	      );distorsionada
	(gimp-selection-feather laImagen 5);Encoger la selección
	(gimp-selection-invert laImagen)
	(gimp-edit-clear capaFondo)
	(gimp-edit-clear capaBrillo)
	(gimp-selection-none laImagen);Deseleccionar todo
	(gimp-edit-fill capaDifu BACKGROUND-FILL)
	(plug-in-hsv-noise 1 laImagen capaTexto 2 180 0 30)
	(plug-in-hsv-noise 1 laImagen capaFondo 2 180 0 15)
	(gimp-image-remove-layer laImagen capaCurro)

)
)
(plug-in-gauss 1 laImagen capaTexto 2 2 0)
;Terminando
(gimp-image-undo-enable laImagen)
(gimp-display-new laImagen)
(gimp-displays-flush)
(gimp-image-clean-all laImagen)
(gimp-context-pop)
))
	(script-fu-register "script-fu-Texto-arrugado"
	_"Texto arrugado"
   	_"Crea un texto con efecto de papel arrugado."
	"Jose Antonio Carrascosa Garcia"
	"Copyright 2014 Jose https://myc-git.gitlab.io/mycweb/index.html"
	"Viernes 31 de octubre de 2014"
	""
	SF-STRING        _"Texto"       "The Gimp"
	SF-FONT          _"Tipo de fuente"       "Georgia Bold Italic"
	SF-COLOR	 _"Color del texto" '(0 0 0)
	SF-ADJUSTMENT    _"Tamaño del texto"  '(120 1 1000 1 10 0 1)
	SF-COLOR         _"Color del papel"      '(240 180 70)
	SF-TOGGLE      _"Envejecer"    FALSE
	)
	(script-fu-menu-register  "script-fu-Texto-arrugado" "<Image>/File/Create")
