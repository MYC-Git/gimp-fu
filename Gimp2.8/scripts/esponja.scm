;  Script inspirado en el tutorial: http://gimpfun.blogspot.com.es/2008/07/spongy-text.html

;***************************************************************************************************************
; La licencia:
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;***************************************************************************************************************


(define (script-fu-Texto-esponjoso inTexto inFuente inFuenteTam sombra )
(let*
    (
	   ; definir las variables locales
	   ; crear la imagen nueva:
	(elAnchodeImagen  10)
	(laAlturadeImagen 10)
	(laImagen (car
			(gimp-image-new
			 elAnchodeImagen
			 laAlturadeImagen
			 RGB
			)
		    )
	 )
	(elTexto)   ;una declaración para el texto.
	(elBufer)
	(gimp-image-undo-disable laImagen)
;Crear una capa nueva en la imagen:
	(capaTexto
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Texto"
			100
			NORMAL
			)
		)
	  )
	(capaFondo
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Plasma"
			100
			NORMAL
			)
		)
	  )
	(capaSombra
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Sombra"
			100
			NORMAL
			)
		)
	  )
	(capaEsponja
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Esponja"
			100
			MULTIPLY
			)
		)
	  )
	) ; fin de las variables locales
(gimp-image-insert-layer laImagen capaTexto 0 0)


(gimp-context-push)
(gimp-context-set-background '(255 255 255) )
(gimp-context-set-foreground '(255 255 255) )
(gimp-drawable-fill capaTexto TRANSPARENT-FILL)
	(set! elTexto
		(car
			(gimp-text-fontname
			laImagen capaTexto
			0 0
			inTexto
			0
			TRUE
			inFuenteTam PIXELS
			inFuente)
		)
	)

	(set! elAnchodeImagen (car (gimp-drawable-width elTexto) ) )
	(set! laAlturadeImagen (car (gimp-drawable-height elTexto) ) )
	(set! elBufer(* laAlturadeImagen(/ 50 100) ) )
	(set! laAlturadeImagen(+ laAlturadeImagen elBufer elBufer) )
	(set! elAnchodeImagen(+ elAnchodeImagen elBufer elBufer) )
	(gimp-image-resize laImagen elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-set-offsets elTexto elBufer elBufer)
	(gimp-layer-resize capaTexto elAnchodeImagen laAlturadeImagen 0 0)


	(set! capaFondo (car (gimp-layer-copy capaTexto TRUE)))
	(gimp-image-insert-layer laImagen capaFondo 0 2)
	(gimp-image-insert-layer laImagen capaEsponja 0 0)
	(gimp-layer-resize capaEsponja elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-item-set-name capaFondo "Plasma")
	(gimp-context-set-background '(255 255 255))
	(gimp-drawable-fill capaFondo BACKGROUND-FILL)
	(gimp-context-set-foreground '(0 0 0) )
	(gimp-drawable-fill capaEsponja FOREGROUND-FILL)

	
;Anclar el texto
	(gimp-floating-sel-anchor elTexto)

;Aplicando los filtros

	(plug-in-mosaic 1 laImagen capaEsponja 5 5 1 0 TRUE 205 0 TRUE TRUE 1 FALSE FALSE)
	(plug-in-plasma 1 laImagen capaFondo 0 1)

	;Preparando la sombra
	(if (= sombra 0)
		(begin
		(gimp-image-insert-layer laImagen capaSombra 0 1)
		(gimp-image-remove-layer laImagen capaSombra))
		(begin
		(set! capaSombra (car (gimp-layer-copy capaTexto TRUE)))
		(gimp-image-insert-layer laImagen capaSombra 0 2)
		(gimp-item-set-name capaSombra "Sombra")
		(gimp-image-select-item laImagen 2 capaSombra)
		(gimp-context-set-background '(0 0 0))
		(gimp-edit-fill capaSombra BACKGROUND-FILL)
		(plug-in-gauss 1 laImagen capaSombra 10 10 0)
		(gimp-layer-set-offsets capaSombra 4 4)
		(gimp-layer-resize-to-image-size capaSombra)
		(gimp-layer-set-opacity capaSombra 80)));Sombra lista

(gimp-selection-none laImagen);Deseleccionar todo	
(gimp-image-undo-enable laImagen)
(gimp-display-new laImagen)
(gimp-image-clean-all laImagen)
(gimp-context-pop)
))
	(script-fu-register "script-fu-Texto-esponjoso"                                   ;nombre de la función 1REQUERIDO
	"Texto esponjoso"                                          ;etiqueta del menú 2REQUERIDO
   	"Crea un texto esponjoso." ;descripción 3REQUERIDO
	"Jose Antonio Carrascosa Garcia"                                                     ;autor 4REQUERIDO
	"Copyright 2015 https://myc-git.gitlab.io/mycweb/"                                      ;copyright 5REQUERIDO
	"Viernes 23 de enero de 2015"                         ;Fecha de creación 6REQUERIDO
	""                                                         ;Tipo de imagen con el que trabaja. 7REQUERIDO
	SF-STRING        _"Texto"       "The Gimp"
	SF-FONT          _"Tipo de fuente"       "Georgia Bold Italic"
	SF-ADJUSTMENT    _"Tamaño del texto"  '(120 1 1000 1 10 0 1)
	SF-TOGGLE        _"Sombra"        0
	)
	(script-fu-menu-register  "script-fu-Texto-esponjoso" "<Image>/File/Create")
