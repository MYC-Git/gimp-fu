;  Script inspirado en el tutorial: http://puteraaladin.blogspot.mx/2008/12/creating-light-text-effect-in-gimp.html

;***************************************************************************************************************
; La licencia:
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;***************************************************************************************************************

(define (script-fu-texto-luz inText fuente tamText inDegr)
(let*
    (
	   ; definir las variables locales
	   ; crear la imagen nueva:
	(elAnchodeImagen  10)
	(laAlturadeImagen 10)
	(laImagen (car
			(gimp-image-new
			 elAnchodeImagen
			 laAlturadeImagen
			 RGB
			)
		    )
	 )
	(elTexto)   ;una declaración para el texto.
	(theBuffer)
	(gimp-image-undo-disable laImagen)
;Crear una capa nueva en la imagen:

	(capaFondo
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Fondo"
			100
			NORMAL
			)
		)
	  )
	(capaTexto
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Texto"
			100
			NORMAL
			)
		)
	  )
	(capaTexto-copia
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Relieve"
			100
			NORMAL
			)
		)
	  )
	(capaNubes
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Nubes"
			100
			OVERLAY-MODE
			)
		)
	  )
	(capaDegradado
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Degradado"
			100
			OVERLAY-MODE
			)
		)
	  )
	(capaColor
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Color"
			100
			OVERLAY-MODE
			)
		)
	  )
	(capaElipse
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Elipse"
			100
			OVERLAY-MODE
			)
		)
	  )
	(capaColor-fondo
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Colorear"
			50
			NORMAL
			)
		)
	  )
	(capaOscurecer
		(car
			(gimp-layer-new
			laImagen
			elAnchodeImagen
			laAlturadeImagen
			RGBA-IMAGE
			"Oscurecer"
			100
			OVERLAY-MODE
			)
		)
	  )
	) ; fin de las variables locales
(gimp-image-insert-layer laImagen capaTexto 0 1)
(gimp-image-insert-layer laImagen capaColor-fondo 0 2)
(gimp-image-insert-layer laImagen capaFondo 0 3)
(gimp-image-insert-layer laImagen capaElipse 0 0)
(gimp-image-insert-layer laImagen capaNubes 0 0)
(gimp-image-insert-layer laImagen capaDegradado 0 0)
(gimp-image-insert-layer laImagen capaColor 0 0)
(gimp-image-insert-layer laImagen capaOscurecer 0 0)

(gimp-context-push)
(gimp-context-set-background '(0 0 0) )
(gimp-context-set-foreground '(255 255 255))
(gimp-context-set-gradient inDegr)
(gimp-drawable-fill capaTexto TRANSPARENT-FILL)

	(set! elTexto
		(car
			(gimp-text-fontname
			laImagen capaTexto
			0 0
			inText
			0
			TRUE
			tamText PIXELS
			fuente)
		)
	)

	(set! elAnchodeImagen (car (gimp-drawable-width elTexto) ) )
	(set! laAlturadeImagen (car (gimp-drawable-height elTexto) ) )
	(set! theBuffer(* laAlturadeImagen(/ 35 100) ) )
	(set! laAlturadeImagen(+ laAlturadeImagen theBuffer theBuffer) )
	(set! elAnchodeImagen(+ elAnchodeImagen theBuffer theBuffer) )
	(gimp-image-resize laImagen elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-set-offsets elTexto theBuffer theBuffer)
	(gimp-layer-resize capaTexto elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaFondo elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaDegradado elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaNubes elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaColor elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaElipse elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaColor-fondo elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-layer-resize capaOscurecer elAnchodeImagen laAlturadeImagen 0 0)
	(gimp-edit-blend capaDegradado CUSTOM-MODE 0 GRADIENT-LINEAR 100 0 REPEAT-NONE FALSE FALSE 0 0 FALSE 0 0 0 laAlturadeImagen);herramienta mezcla
	(gimp-drawable-fill capaFondo BACKGROUND-FILL)
	(gimp-drawable-fill capaOscurecer BACKGROUND-FILL)
	(gimp-drawable-fill capaNubes FOREGROUND-FILL)
	(gimp-context-set-foreground '(255 32 27))
	(gimp-drawable-fill capaColor FOREGROUND-FILL)
	(gimp-edit-blend capaColor-fondo 2 0 GRADIENT-RADIAL 100 0 REPEAT-NONE FALSE FALSE 0 0 FALSE 0 0 (/ elAnchodeImagen 2) laAlturadeImagen)
	(gimp-drawable-fill capaElipse TRANSPARENT-FILL)
	(gimp-image-select-ellipse laImagen 2 (/ elAnchodeImagen 6) (/ laAlturadeImagen 4) (/ elAnchodeImagen 3) (/ laAlturadeImagen 3))
	(gimp-context-set-foreground '(255 255 255))
	(gimp-edit-fill capaElipse FOREGROUND-FILL)
	(gimp-selection-none laImagen)
	(plug-in-solid-noise 1 laImagen capaNubes TRUE FALSE 0 4 4 4)
	(gimp-floating-sel-anchor elTexto)
	(gimp-image-select-item laImagen 2 capaTexto)
	(gimp-selection-border laImagen 2)
	(gimp-selection-invert laImagen)
	(gimp-edit-clear capaTexto)
	(gimp-selection-none laImagen)
	(plug-in-gauss 1 laImagen capaTexto 2 2 1)
	(set! capaTexto-copia (car (gimp-layer-copy capaTexto TRUE )));copiar la capa
	(gimp-image-insert-layer laImagen capaTexto-copia 0 -1)
	(gimp-layer-resize capaTexto-copia elAnchodeImagen laAlturadeImagen 0 0)
	(plug-in-gauss 1 laImagen capaTexto-copia 5 5 1)
	(plug-in-gauss 1 laImagen capaElipse 90 90 1)
(gimp-image-undo-enable laImagen)
(gimp-display-new laImagen)
(gimp-displays-flush)
(gimp-image-clean-all laImagen)
(gimp-context-pop)
))
	(script-fu-register "script-fu-texto-luz"                                   ;nombre de la función 1REQUERIDO
	"Texto de luz"                                          ;etiqueta del menú 2REQUERIDO
   	"Texto luminoso" ;descripción 3REQUERIDO
	"Jose Antonio Carrascosa Garcia"                                                     ;autor 4REQUERIDO
	"Copyright 2015 Jose https://myc-git.gitlab.io/mycweb/index.html"                                      ;copyright 5REQUERIDO
	"Lunes 5 de enero de 2015"                         ;Fecha de creación 6REQUERIDO
	""                                                         ;Tipo de imagen con el que trabaja. 7REQUERIDO
	SF-STRING        _"Texto"       "Texto de luz"                  ;Variable de cadena.
	SF-FONT          _"Tipo de fuente"       "Arial Black"          ;Variable tipografia.
	SF-ADJUSTMENT    _"Tamaño del texto"  '(100 1 1000 1 10 0 1)  ;boton de giro
	SF-GRADIENT         _"Degradado"      "Incandescent"                     ;Variable color.
	)
	(script-fu-menu-register  "script-fu-texto-luz" "<Image>/File/Create/")
